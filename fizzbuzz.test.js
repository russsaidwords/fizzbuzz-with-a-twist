const fizzBuzz = require('./fizzbuzz')

test('expects n=1 to result in "1" at first element', () => {
  expect(fizzBuzz(5)[0]).toStrictEqual('1')
})

test('expects n=3 to result in "Fizz" at third element', () => {
  expect(fizzBuzz(3)[2]).toStrictEqual('Fizz')
})

test('expects n=5 to result in "Buzz" at fifth element', () => {
  expect(fizzBuzz(5)[4]).toStrictEqual('Buzz')
})

test('expects n=15 to result in "FizzBuzz" at fifteenth element', () => {
  expect(fizzBuzz(15)[14]).toStrictEqual('FizzBuzz')
})

test('expects n=29 to result in "29" at 29th element', () => {
  expect(fizzBuzz(29)[28]).toStrictEqual('29')
})

test('expects n=30 to result in "FizzBuzz" at 30th element', () => {
  expect(fizzBuzz(30)[29]).toStrictEqual('FizzBuzz')
})

test('expects n=90 to result in "FizzBuzz" at 90th element', () => {
  expect(fizzBuzz(90)[89]).toStrictEqual('FizzBuzz')
})
