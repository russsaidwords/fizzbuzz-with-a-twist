# FizzBuzz with a Twist

![pipeline](https://gitlab.com/russsaidwords/fizzbuzz-with-a-twist/badges/master/pipeline.svg)
![coverage](https://gitlab.com/russsaidwords/fizzbuzz-with-a-twist/badges/master/coverage.svg?job=test_coverage)

This is a simple attempt at a FizzBuzz challenge with a twist: no "if" or "?" operator allowed.

Testing and CI were implemented as an extra exercise.