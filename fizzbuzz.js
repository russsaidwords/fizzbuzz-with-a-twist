function fizzBuzz (n) {
  let i = 1
  let fbline = []
  const output = []
  let count = 0
  while (i <= n) {
    fbline = [i, i, i, 'Fizz', i, 'Buzz', 'Fizz', i, i, 'Fizz', 'Buzz', i, 'Fizz', i, i, 'FizzBuzz']
    output.push('' + fbline[(i + count) % 16])
    i++
    count = Math.floor(i / 16)
  }
  return output
}

module.exports = fizzBuzz
